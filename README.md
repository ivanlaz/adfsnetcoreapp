# README #

This code repository contains an ASP.NET Core sample app which is protected by ADFS 2016 using OpenID Connect. It is based off of the [Azure AD ASP.NET Core sample](https://github.com/Azure-Samples/active-directory-dotnet-webapp-openidconnect-aspnetcore) and is adapted to work with ADFS 2016 on premises.

To set up OpenID Connect on ADFS, the steps outlined in [this article](https://technet.microsoft.com/en-us/windows-server-docs/identity/ad-fs/development/enabling-openid-connect-with-ad-fs-2016) were followed.

### Testing ###
The sample app is expected to run on http://localhost:5000. Please ensure that you debug on that host and port. ADFS will attempt to complete the flow using the callback URL http://localhost:5000/signin-oidc.

The following login details can be used to test the login:

- User: **testuser@bbdtest.co.za**
- Password: **Password@1**

### Useful links ###
- https://rimdev.io/openid-connect-and-asp-net-core-1-0/